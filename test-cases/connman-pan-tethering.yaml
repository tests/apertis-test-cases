metadata:
  name: connman-pan-tethering
  format: "Apertis Test Definition 1.0"
  image-types:
    hmi:     [ armhf, arm64, amd64 ]
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
    - OSTree
  type: functional
  exec-type: manual
  priority: medium
  maintainer: "Apertis Project"
  description: "Test ConnMan support for PAN connectivity."

  resources:
    - "A Bluetooth adapter."
    - "A device supporting PAN PANU."

  macro_ostree_preconditions:
    reponame: connman-pan-tethering
    branch: apertis/v2026dev2

  pre-conditions:
    - "Make sure an Internet connection is active on the system."
    - "When testing on the SDK image, make sure blueman-applet is not running or
       kill it."

  expected:
    - "If connections succeeds and you are able to navigate on the internet then
       it works. The output should be similar to that:"
    - |
        >select_adapter
        Selected /org/bluez/hci0
        select_device: Discovering...
        Device found: F0:39:42:86:2E:1B Charge 2
        Device found: 88:83:22:2F:56:2A Galaxy A5 2016
        Input device address: 88:83:22:2F:56:2A
        Selected address: 88:83:22:2F:56:2A
        start_agent
        test_pairing_initiator
        Scanning in progress...
        Pairing request to 88:83:22:2F:56:2A in progress...
        Device found: /org/bluez/hci0/dev_88_83_22_2F_56_2A
        Device 88_83_22_2F_56_2A is paired
        test_pairing_responder
        Start a pairing from the phone! was it successful (y/n):
        y
        Device found: /org/bluez/hci0/dev_88_83_22_2F_56_2A
        Device 88_83_22_2F_56_2A is paired

  notes:
    - "Warning: Connman changes are persistent (over reboot!). After finishing
       testing, it might be wise to perform the dhcp test to ensure that the
       network is in a sensible state."
    - "For ALL tests the enable step will give an \"In progress\" error if the
       device is already enabled. This is to be expected."

run:
  steps:
    - "Enable bluetooth:"
    - $ connmanctl enable bluetooth
    - "Start Bluetooth tethering:"
    - $ connmanctl tether bluetooth on
    - "The phone must be in discoverable mode for starting this test. Look for
       the phone in this list, and save the BT address. It will be used while
       running the test."
    - "Execute the test suite inside an environment with dbus:"
    - $ ./connman-pan-tethering.sh
    - "There are some options:"
    - |
        >-a select which device you want to pair with (specify the address)
        -s skip pairing because the device is already paired. -a must be present when using this.
        -x enables test debugging (only useful if a test fails)
    
    - "On the other device, scan for the Bluetooth device and pair with, then
       connect to the Bluetooth PAN. Most phones(?) don't actually support
       tethering to another device (as they, by definition, already have an
       internet connection) but connecting a computer and asking, say,
       NetworkManager to connect to the newly paired bluetooth device should work
       just fine (when testing in this case, ensure all other connections are
       disabled!)"

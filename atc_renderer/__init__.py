###################################################################################
# Main program to render test case files.
# Create a HTML page from a YAML test case file.
#
# Copyright (C) 2018
# Luis Araujo <luis.araujo@collabora.co.uk>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import os
import shutil
import pkg_resources

from atc_renderer.renderer import generate_test_case_page, \
    generate_index_page, \
    PYTHON_PKGNAME

def copy_files(directory, dirname, dirpath, msg):
    """
    Only copy files if destination directory (dst_dir) is not in the cwd.
    """
    dst_dir = os.path.join(os.path.realpath(directory), dirname)
    if not os.path.isdir(dst_dir):
        print("Creating directory", dst_dir)
        os.mkdir(dst_dir)

    print(msg, dst_dir)
    files = os.listdir(dirpath)
    for f in files:
        shutil.copy2(os.path.join(dirpath, f), dst_dir)


def main(tc_files, tc_dir = None, index_page = False):
    index_files = []
    directory = os.getcwd()
    if tc_dir:
        directory = tc_dir
        try:
            os.mkdir(directory)
        except FileExistsError:
            print("Directory '{}' already exists".format(directory))
        except e:
            print("Error:", e)
            exit(1)

    if os.path.isfile(tc_files):
        generate_test_case_page(tc_files, directory)
        if index_page:
            index_files.append(tc_files)
    else:
        c = 0
        for root, _, files in os.walk(tc_files):
            for f in files:
                tc_file = os.path.join(root, f)
                if os.path.isfile(tc_file):
                    generate_test_case_page(tc_file, directory)
                    if index_page:
                        index_files.append(tc_file)
                    c += 1
        print("Total of test cases processed:", c)

    # Copy CSS and images directory
    css_dir = pkg_resources.resource_filename(PYTHON_PKGNAME, 'css/')
    images_dir = pkg_resources.resource_filename(PYTHON_PKGNAME, 'images/')
    copy_files(directory, 'css/', css_dir, "Copying css style to")
    copy_files(directory, 'images/', images_dir, "Copying images to")

    if index_files:
        generate_index_page(index_files, directory)
